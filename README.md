# README #
Description

	This is a simple REST account transfer service. There are 2 main entry points into the system, namely:
	
	1- Account api 
		functionality to create a new monetary account
		functionality to list account by account number  
	
	2- Transfer api
		functionality to transfer from one account to another
		functionality to list transfers by account number  
	
Technology used

	Spring boot
	REST web service
	Mysql database
	JPA Hibernate 
	JSON 
	Maven build tool
	Swagger documentation

Getting Started
	
	1. A mysql database needs to be created
		  e.g CREATE database ingenico
	
	2. Edit application properties 
			2.1 edit the following properties with the database credentials 	
				spring.datasource.url=jdbc:mysql://localhost:3306/??????
				spring.datasource.username=?????
				spring.datasource.password=?????
			2.2 the application will start up on port 8080, you can edit this by changing server.port in application.properties 
				e.g. server.port = 8082
				
	3. Create the Account and Transfer tables by running the below commands against the db you created in step 1 
	
  			CREATE TABLE Account (id bigint(20) NOT NULL AUTO_INCREMENT,
  			name varchar(40) NOT NULL,
  			balance bigint(20),
  			accountNumber varchar(20),
  			createdDate  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			PRIMARY KEY (id));
   
  		    CREATE TABLE Transfer (id bigint(20) NOT NULL AUTO_INCREMENT,
  			fromAccountNumber varchar(20) NOT NULL,
  			toAccountNumber varchar(20) NOT NULL,
  			dateCreated  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			amount bigint(20),  
  			PRIMARY KEY (id));
  			
	4. Application should be stated and ready to use
	
	
Steps to interact with service
		
		The user can interact with the service by going to the following url- http://localhost:8080/swagger-ui.html
		Please note to change the port (8080) to the one configured in the application.properties file 
		
		
		Test scenario
		-------------
		During all the subsequent steps appropriate validation messages will be returned to guide the user
		
		Step 1. Create 2 accounts by following the  
			 1.1 account-controller -[POST] /api/account/create  
		  	 
			  	Example- 
			  			accountCreateRequest {
  												"account_name": "John",
  												"balance_in_cents": 65000
									 		 } 
								
								
 						Response Body {
 										"account_name": "John",
  							   			"balance_in_cents": 65000,
  							   			"account_number": "203309041"
							  }
							  
			    NB! Please note that the account number will now be used in subsequent 
								
		  	  1.2 You can validate that the accounts have been created by calling -[GET] /api/account/lookup/accountNumber/{accountNumber}
		
		
		Step 2. With the account numbers generated from step 1 initiate a transfer request 
			 2.1 transfer-controller  - [POST] /api/transfer/funds 
			 
			 	 Example- 
						transferRequest {
  											"amount_in_cents": 1500,
  											"from_account_number": "380392052",
  											"to_account_number": "203309041"
										}
										
						 Response Code 200
						 
			2.2 Verify that the transfer has taken place by calling - [GET] /api/transfer/lookup/accountNumber/{accountNumber}
		 		The account number should the fromAccountNumber
		 		
		 		Response Body {
  								"transfers": [
    							 {
      								"id": 9,
      								"fromAccountNumber": "380392052",
      								"toAccountNumber": "203309041",
      								"dateCreated": 1516187046000,
      								"amount": 1500
    							}
  							   ]
							  }
   				
   				Note the account balances have changed