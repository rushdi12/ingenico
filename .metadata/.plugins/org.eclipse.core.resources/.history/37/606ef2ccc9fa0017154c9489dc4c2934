package com.ingenico.transfer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ingenico.transfer.dto.AccountCreateRequest;
import com.ingenico.transfer.dto.TransferRequest;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ServicesRuntimeException;
import com.ingenico.transfer.exception.ValidationException;
import com.ingenico.transfer.manager.AccountManager;
import com.ingenico.transfer.manager.TransferManager;
import com.ingenico.transfer.models.Account;
import com.ingenico.transfer.utils.StringUtilities; 

@Controller
@RequestMapping("/api/transfer") 
@Produces(MediaType.APPLICATION_JSON_VALUE)
public class TransferController {

	@Autowired
	private AccountManager accountManager;
	
	@Autowired
	private TransferManager transferManager;
	
	private final Logger log = LoggerFactory.getLogger(TransferController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> transfer(@RequestBody TransferRequest transferRequest,
										   HttpServletRequest httpReq,
										   HttpServletResponse httpResp) {

		ResponseEntity<TransferRequest> response = null;
		AccountCreateRequest accountCreateResponse = null; 
		try {
			validateRequest(transferRequest);
			transferManager.accountTransfer(transferRequest.getFromAccount(), transferRequest.getToAccount(), transferRequest.getAmountInCents());

			response = new ResponseEntity<AccountCreateRequest>(accountCreateResponse, HttpStatus.OK);
		} catch (ValidationException ve) {
			throw ve;
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
			throw e;
		}  

		return response;
	}
	
	
	private void validateRequest(TransferRequest transferRequest){
		
		if(StringUtilities.isEmpty(transferRequest.getFromAccount()) ||
		   StringUtilities.isEmpty(transferRequest.getToAccount()) ||
		   transferRequest.getAmountInCents() <= 0){
			
			throw new ValidationException(HttpStatus.OK, ErrorCodes.ING003);
		}

		
		Account fromAccount = accountManager.lookUpAccount(transferRequest.getFromAccount());
		 
		if(fromAccount == null)
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING001);
		
		if(fromAccount.getBalance() < transferRequest.getAmountInCents())
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING002);
		
			
		if(accountManager.lookUpAccount(transferRequest.getToAccount()) == null)
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING001);
	}
	
	
	
	
}
