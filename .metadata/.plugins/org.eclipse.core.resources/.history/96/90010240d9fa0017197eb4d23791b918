package com.ingenico.transfer.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenico.transfer.dto.TransferRequest;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ServicesRuntimeException;
import com.ingenico.transfer.manager.TransferManager;
import com.ingenico.transfer.models.Account;
import com.ingenico.transfer.models.Transfer;
import com.ingenico.transfer.repo.AccountRepository;
import com.ingenico.transfer.repo.TransferRepository;

@Service
public class TransferManagerImpl implements TransferManager {

	@Autowired
	private TransferRepository transferRepo;
	
	@Autowired
	private AccountRepository accountRepo;
	
	private Account fromAccount;
	private Account toAccount;
	
	@Override
	@Transactional
	public void accountTransfer(TransferRequest request) {
		 	
		verifyAccountDetails(request);
		
		Transfer transfer = new Transfer();
		transfer.setFromAccount(request.getFromAccount());
		transfer.setToAccount(request.getToAccount());
		transfer.setAmount(request.getAmountInCents());
		
		transferRepo.save(transfer);
		
		updateAcountAmount(request);
		
	}
	
	public void verifyAccountDetails(TransferRequest request){

		fromAccount = accountRepo.findByName(request.getFromAccount());
		toAccount = accountRepo.findByName(request.getToAccount());
		
		if(fromAccount == null || toAccount == null)
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING005);
		
		if(fromAccount.getBalance() < request.getAmountInCents())
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING002);
		
		
	}
	
	private void updateAcountAmount(TransferRequest request) {

		Long transactionAmount = request.getAmountInCents();

		Long fromAccountUpdatedAmount = fromAccount.getBalance() - transactionAmount;
		fromAccount.setBalance(fromAccountUpdatedAmount);
		accountRepo.save(fromAccount);

		Long toAccountUpdatedAmount = toAccount.getBalance() + transactionAmount;
		toAccount.setBalance(toAccountUpdatedAmount);
		accountRepo.save(toAccount);

	}
}
