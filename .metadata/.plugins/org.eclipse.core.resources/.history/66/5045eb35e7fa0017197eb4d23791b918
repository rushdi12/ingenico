package com.ingenico.transfer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ingenico.transfer.dto.AccountCreateRequest;
import com.ingenico.transfer.dto.TransferRequest;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ServicesRuntimeException;
import com.ingenico.transfer.exception.ValidationException;
import com.ingenico.transfer.manager.AccountManager;
import com.ingenico.transfer.models.Account;
import com.ingenico.transfer.utils.StringUtilities; 

@Controller
@RequestMapping("/api/account") 
@Produces(MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

	@Autowired
	private AccountManager accountManager;
	
	private final Logger log = LoggerFactory.getLogger(AccountController.class);
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> createAccount(@RequestBody AccountCreateRequest accountCreateRequest,
										   HttpServletRequest httpReq,
										   HttpServletResponse httpResp) {

		ResponseEntity<AccountCreateRequest> response = null; 
		
		try {
 
			//validate incoming request 
			validateIncomingRequest(accountCreateRequest);
			
			Account newAcount = accountManager.createAccount(accountCreateRequest.getName(), accountCreateRequest.getBalanceInCents());
			accountCreateRequest.setAccountNumber(newAcount.getAccountNumber());
			
			response = new ResponseEntity<AccountCreateRequest>(accountCreateRequest, HttpStatus.OK);
		 
			
		} catch (ValidationException ve) {
			throw ve;
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
			throw e;
		}

		return response;
	}
	
	private void validateIncomingRequest(AccountCreateRequest accountCreateRequest) {

		if (StringUtilities.isEmpty(accountCreateRequest.getName()))
			throw new ValidationException(HttpStatus.OK, ErrorCodes.ING003);
		 
	}
	
}
