package com.ingenico.transfer.repo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenico.transfer.models.Transfer;
 
 
@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> {
 
	List<Transfer> findAllByFromAccountNumber(String accountNumber);
}
