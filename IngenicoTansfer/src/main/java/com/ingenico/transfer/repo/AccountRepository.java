package com.ingenico.transfer.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ingenico.transfer.models.Account;
 
 
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	public Account findByName(String name);	
	
	public Account findByAccountNumber(String accountNumber);	
	
}
