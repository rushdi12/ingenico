package com.ingenico.transfer.manager;

import com.ingenico.transfer.dto.TransferLookupResponse;
import com.ingenico.transfer.dto.TransferRequest;

public interface TransferManager {
	
	public void accountTransfer(TransferRequest request);

	public TransferLookupResponse lookUpTransferByAccountNumber(String accountNumber);
}
