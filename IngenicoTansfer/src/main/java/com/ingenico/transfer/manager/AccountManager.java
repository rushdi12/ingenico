package com.ingenico.transfer.manager;

import com.ingenico.transfer.dto.AccountLookupResponse;
import com.ingenico.transfer.models.Account;

public interface AccountManager {
	
	public Account createAccount(String accountName, Long balance);
	
	public AccountLookupResponse lookUpAccount(String accountNumber);
}
