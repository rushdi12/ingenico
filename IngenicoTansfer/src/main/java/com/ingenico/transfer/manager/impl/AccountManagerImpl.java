package com.ingenico.transfer.manager.impl;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenico.transfer.dto.AccountLookupResponse;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ServicesRuntimeException;
import com.ingenico.transfer.manager.AccountManager;
import com.ingenico.transfer.models.Account;
import com.ingenico.transfer.repo.AccountRepository;

@Service
public class AccountManagerImpl implements AccountManager{

	@Autowired
	private AccountRepository accountRepo;
	
	
	@Override
	@Transactional
	public Account createAccount(String name, Long balance) {

		Account account = new Account();
		account.setName(name);
		account.setBalance(balance);

		Random rand = new Random();
		int n = rand.nextInt(900000000) + 100000000;

		account.setAccountNumber(String.valueOf(n));

		return accountRepo.save(account);
		
	}

	@Override
	public AccountLookupResponse lookUpAccount(String accountNumber) {
		Account account = accountRepo.findByAccountNumber(accountNumber);
		
		if(account == null)
			throw new ServicesRuntimeException(HttpStatus.BAD_REQUEST, ErrorCodes.ING004);
		
		AccountLookupResponse response = new AccountLookupResponse();
		
		response.setAccountNumber(account.getAccountNumber());
		response.setName(account.getName());
		response.setBalanceInCents(account.getBalance());
		
		return response;
	}

}
