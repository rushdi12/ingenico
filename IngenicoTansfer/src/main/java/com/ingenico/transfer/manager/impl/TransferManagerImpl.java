package com.ingenico.transfer.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingenico.transfer.dto.TransferLookupResponse;
import com.ingenico.transfer.dto.TransferRequest;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ServicesRuntimeException;
import com.ingenico.transfer.manager.TransferManager;
import com.ingenico.transfer.models.Account;
import com.ingenico.transfer.models.Transfer;
import com.ingenico.transfer.repo.AccountRepository;
import com.ingenico.transfer.repo.TransferRepository;

@Service
public class TransferManagerImpl implements TransferManager {

	@Autowired
	private TransferRepository transferRepo;
	
	@Autowired
	private AccountRepository accountRepo;
	
	@Override
	@Transactional
	public void accountTransfer(TransferRequest request) {

		Account fromAccount = accountRepo.findByAccountNumber(request.getFromAccountNumber());
		Account toAccount = accountRepo.findByAccountNumber(request.getToAccountNumber());
		
		verifyAccountDetails(request, fromAccount, toAccount);
		
		recordTransction(request);
		
		updateAccountAmount(request, fromAccount, toAccount);
		
	}
	
	public void verifyAccountDetails(TransferRequest request, Account fromAccount, Account toAccount){
		
		if(fromAccount == null )
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING004, " [From Account] ");
		if(toAccount == null)
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING004, " [To Account] ");
		if(fromAccount.getBalance() < request.getAmountInCents())
			throw new ServicesRuntimeException((HttpStatus.OK), ErrorCodes.ING002);
	}
	
	public void recordTransction(TransferRequest request) {
		Transfer transfer = new Transfer();
		
		transfer.setFromAccountNumber(request.getFromAccountNumber());
		transfer.setToAccountNumber(request.getToAccountNumber());
		transfer.setAmount(request.getAmountInCents());

		transferRepo.save(transfer);
	}
	
	private void updateAccountAmount(TransferRequest request, Account fromAccount, Account toAccount) {

		Long transactionAmount = request.getAmountInCents();

		Long fromAccountUpdatedAmount = fromAccount.getBalance() - transactionAmount;
		fromAccount.setBalance(fromAccountUpdatedAmount);
		accountRepo.save(fromAccount);

		Long toAccountUpdatedAmount = toAccount.getBalance() + transactionAmount;
		toAccount.setBalance(toAccountUpdatedAmount);
		accountRepo.save(toAccount);

	}

	@Override
	@Transactional
	public TransferLookupResponse lookUpTransferByAccountNumber(String accountNumber) {
		
		TransferLookupResponse transferResponse = new TransferLookupResponse();
		
		List<Transfer> transfers = transferRepo.findAllByFromAccountNumber(accountNumber);
		
		transferResponse.setTransfers(transfers);
		
		return transferResponse;
		 
	}
}
