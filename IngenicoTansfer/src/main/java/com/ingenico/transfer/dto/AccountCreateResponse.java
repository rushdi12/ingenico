package com.ingenico.transfer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountCreateResponse {
	
	@JsonProperty("account_name")
	private String accountName;

	@JsonProperty("balance_in_cents")
	private Long balanceInCents;
	
	@JsonProperty("account_number")
	private String accountNumber;
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Long getBalanceInCents() {
		return balanceInCents;
	}

	public void setBalanceInCents(Long balanceInCents) {
		this.balanceInCents = balanceInCents;
	}

}
