package com.ingenico.transfer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountCreateRequest {

	@JsonProperty("account_name")
	private String accountName;

	@JsonProperty("balance_in_cents")
	private Long balanceInCents;

	public Long getBalanceInCents() {
		return balanceInCents;
	}

	public void setBalanceInCents(Long balanceInCents) {
		this.balanceInCents = balanceInCents;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
 
}
