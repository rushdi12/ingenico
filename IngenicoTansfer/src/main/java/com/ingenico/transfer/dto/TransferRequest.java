package com.ingenico.transfer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferRequest {

	@JsonProperty("from_account_number")
	private String fromAccountNumber;

	@JsonProperty("to_account_number")
	private String toAccountNumber;
	
	@JsonProperty("amount_in_cents")
	private Long amountInCents;

	public Long getAmountInCents() {
		return amountInCents;
	}

	public void setAmountInCents(Long amountInCents) {
		this.amountInCents = amountInCents;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(String toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}
	
	 
}
