package com.ingenico.transfer.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ingenico.transfer.models.Transfer;

public class TransferLookupResponse {

	@JsonProperty("transfers")
	private List<Transfer> transfers;

	public List<Transfer> getTransfers() {
		return transfers;
	}

	public void setTransfers(List<Transfer> transfers) {
		this.transfers = transfers;
	}

}
