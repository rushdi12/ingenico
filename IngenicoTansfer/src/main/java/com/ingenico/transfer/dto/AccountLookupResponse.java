package com.ingenico.transfer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountLookupResponse {

	@JsonProperty("name")
	private String name;

	@JsonProperty("account_number")
	private String accountNumber;
	
	@JsonProperty("balance_in_cents")
	private Long balanceInCents;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Long getBalanceInCents() {
		return balanceInCents;
	}

	public void setBalanceInCents(Long balanceInCents) {
		this.balanceInCents = balanceInCents;
	}

	 
}
