package com.ingenico.transfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngenicoTansferApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngenicoTansferApplication.class, args);
	}
}
