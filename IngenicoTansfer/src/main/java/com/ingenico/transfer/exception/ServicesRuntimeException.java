package com.ingenico.transfer.exception;

import org.springframework.http.HttpStatus;

import com.ingenico.transfer.enums.ErrorCodes;

public class ServicesRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 49074301554845822L;

	private HttpStatus httpCode;
	private ErrorCodes error;
	private String displayMessage;

	public ServicesRuntimeException(HttpStatus httpCode, ErrorCodes error, String displayMessage) {
		super();
		this.httpCode = httpCode;
		this.error = error;
		this.displayMessage = displayMessage;
	}
	
	public ServicesRuntimeException(HttpStatus httpCode, ErrorCodes error) {
		super();
		this.httpCode = httpCode;
		this.error = error;
	}
	
	
	public ServicesRuntimeException(Throwable t) {
		super(t);
		httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
		error = ErrorCodes.ING999;
	}

	public ServicesRuntimeException() {
		super(); 
		httpCode = HttpStatus.INTERNAL_SERVER_ERROR;
		error = ErrorCodes.ING999;
	}

	public HttpStatus getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(HttpStatus httpCode) {
		this.httpCode = httpCode;
	}  
	
	public ErrorCodes getError() {
		return error;
	}

	public void setError(ErrorCodes error) {
		this.error = error;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	
}
