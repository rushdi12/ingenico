package com.ingenico.transfer.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.enums.ServiceError;

@ControllerAdvice
public class TransferExceptionHandler {

	@ExceptionHandler(ServicesRuntimeException.class)
    @ResponseBody
    public ServiceError handleServicesException(HttpServletRequest httpReq, HttpServletResponse httpResp, ServicesRuntimeException sre) {

        httpResp.setStatus(sre.getHttpCode().value());

        ServiceError error = new ServiceError();
    	error.setCode(sre.getError().getCode());
    	error.setMessage(sre.getError().getMessage() + (sre.getDisplayMessage() == null ? "" : sre.getDisplayMessage()));
    	return error;

    }

	@ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ServiceError handleValidationException(HttpServletRequest httpReq, HttpServletResponse httpResp, ValidationException ve) {

        httpResp.setStatus(ve.getHttpCode().value());

        ServiceError error = new ServiceError();
    	error.setCode(ve.getError().getCode());
    	error.setMessage(ve.getError().getMessage() + ve.getDisplayMessage());
    	return error;

    }

	@ExceptionHandler(Throwable.class)
    @ResponseBody
    public ServiceError handleThrowable(HttpServletRequest httpReq, HttpServletResponse httpResp, Throwable t) {

        httpResp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

        ServiceError error = new ServiceError();
    	error.setCode(ErrorCodes.ING999.getCode());
    	error.setMessage(ErrorCodes.ING999.getMessage() + " " + t.getMessage());
    	return error;
    }

}
