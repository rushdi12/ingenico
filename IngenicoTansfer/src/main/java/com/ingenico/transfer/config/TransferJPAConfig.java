package com.ingenico.transfer.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class TransferJPAConfig {

	// JDBC Configurations
	private final String SPRING_DATASOURCE_URL = "spring.datasource.url";
	private final String SPRING_DATASOURCE_USERNAME = "spring.datasource.username";
	private final String SPRING_DATASOURCE_PASSWORD = "spring.datasource.password";
	private final String SPRING_DATASOURCE_DRIVER_CLASSNAME = "spring.datasource.driver.class";

	// Hikari Configuration Names
	private final String HIKARI_MIN_IDLE = "spring.datasource.hikaricp.minimumIdle";
	private final String HIKARI_MAX_POOLSIZE = "spring.datasource.hikaricp.maxPoolSize";
	private final String HIKARI_IDLE_TIMEOUT = "spring.datasource.hikaricp.maxIdleTime";

	@Bean
	public DataSource dataSource(Environment env) {
		HikariConfig dsConfig = new HikariConfig();

		Properties datasourceProperties = new Properties();

		dsConfig.setUsername(env.getRequiredProperty(SPRING_DATASOURCE_USERNAME));
		dsConfig.setPassword(env.getRequiredProperty(SPRING_DATASOURCE_PASSWORD));
		dsConfig.setJdbcUrl(env.getRequiredProperty(SPRING_DATASOURCE_URL));
		dsConfig.setDriverClassName(env.getRequiredProperty(SPRING_DATASOURCE_DRIVER_CLASSNAME));

		dsConfig.setDataSourceProperties(datasourceProperties);

		HikariDataSource ds = new HikariDataSource(dsConfig);

		ds.setMinimumIdle(Integer.parseInt(env.getProperty(HIKARI_MIN_IDLE)));
		ds.setMaximumPoolSize(Integer.parseInt(env.getProperty(HIKARI_MAX_POOLSIZE)));
		ds.setIdleTimeout(Integer.parseInt(env.getProperty(HIKARI_IDLE_TIMEOUT)));

		ds.setRegisterMbeans(true);

		return ds;
	}

}
