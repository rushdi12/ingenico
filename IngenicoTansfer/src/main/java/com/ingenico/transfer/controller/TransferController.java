package com.ingenico.transfer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.transfer.dto.TransferLookupResponse;
import com.ingenico.transfer.dto.TransferRequest;
import com.ingenico.transfer.enums.ErrorCodes;
import com.ingenico.transfer.exception.ValidationException;
import com.ingenico.transfer.manager.TransferManager;
import com.ingenico.transfer.utils.StringUtilities;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/transfer")
@Produces(MediaType.APPLICATION_JSON_VALUE)
@Api(value="Transfer operations api")
public class TransferController {

	@Autowired
	private TransferManager transferManager;

	private final Logger log = LoggerFactory.getLogger(TransferController.class);

	@ApiOperation(value = "Transfer amount from one account to another",response = ResponseEntity.class)
	@RequestMapping(value = "/funds", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> transfer(@RequestBody TransferRequest transferRequest, 
									  HttpServletRequest httpReq,
									  HttpServletResponse httpResp) {

		ResponseEntity<TransferRequest> response = null;
	 		
		try {
			
			validateIncomingRequest(transferRequest);
			
			transferManager.accountTransfer(transferRequest);

			response = new ResponseEntity<TransferRequest>(HttpStatus.OK);

		} catch (ValidationException ve) {
			throw ve;
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
			throw e;
		}

		return response;
	}
	
	@ApiOperation(value = "Lookup transfer details by account number",response = ResponseEntity.class)
	@RequestMapping(value = "/lookup/accountNumber/{accountNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> lookupTransferDetailsByAccoungtNumber(@PathVariable("accountNumber") String accountNumber,
													   HttpServletRequest httpReq, 
													   HttpServletResponse httpResp) {
		
		return new ResponseEntity<TransferLookupResponse>(transferManager.lookUpTransferByAccountNumber(accountNumber), HttpStatus.OK);
		
	}
	

	private void validateIncomingRequest(TransferRequest transferRequest) {

		if (StringUtilities.isEmpty(transferRequest.getFromAccountNumber()))
			throw new ValidationException(HttpStatus.BAD_REQUEST, ErrorCodes.ING003, " [From account number missing] ");

		if (StringUtilities.isEmpty(transferRequest.getToAccountNumber())) 
			throw new ValidationException(HttpStatus.BAD_REQUEST, ErrorCodes.ING003, " [To account number missing] ");	

		if (transferRequest.getAmountInCents() <= 0)
			throw new ValidationException(HttpStatus.BAD_REQUEST, ErrorCodes.ING003, " [Transfer amount must be greater than 0] ");	

		if(transferRequest.getFromAccountNumber().equalsIgnoreCase(transferRequest.getToAccountNumber()))
			throw new ValidationException((HttpStatus.BAD_REQUEST), ErrorCodes.ING003, " [You cannot transfer to the same account ] ");
	}

}
